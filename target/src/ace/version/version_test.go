package version

import (
	"sort"
	"testing"
)

func TestNew(t *testing.T) {
	v1 := ParseVersion("0.0.0")
	if !EmptyVersion.Equal(v1) {
		t.Errorf("Expected %s, but got %s", EmptyVersion, v1)
	}

	v2 := ParseVersion("1.0.0")
	if v2.Major != 1 || v2.Minor != 0 || v2.Micro != 0 {
		t.Errorf("Expected 1.0.0, but got %s", v2)
	}

	v3 := ParseVersion("2.1")
	if v3.Major != 2 || v3.Minor != 1 || v3.Micro != 0 {
		t.Errorf("Expected 2.1.0, but got %s", v3)
	}
}

func TestParseVersions(t *testing.T) {
	expected := []Version{
		{3, 1, 0},
		{2, 5, 0},
		{3, 0, 0},
		{1, 1, 0},
		{2, 0, 0},
		{1, 0, 0},
	}

	versions := ParseVersions("3.1.0\n2.5.0\n3.0.0\n1.1.0\n2.0.0\n1.0.0")
	for i := 0; i < len(expected); i++ {
		if !expected[i].Equal(versions[i]) {
			t.Errorf("Parsing failed at index %d", i)
		}
	}
}

func TestSort(t *testing.T) {
	versions := []Version{
		{3, 1, 0},
		{2, 5, 0},
		{3, 0, 0},
		{1, 1, 0},
		{2, 0, 0},
		{1, 0, 0},
	}

	sort.Sort(Versions(versions))

	if !(Version{1, 0, 0}).Equal(versions[0]) {
		t.Errorf("Expected 1.0.0: %s", versions[0])
	}
	if !(Version{1, 1, 0}).Equal(versions[1]) {
		t.Errorf("Expected 1.1.0: %s", versions[1])
	}
	if !(Version{2, 0, 0}).Equal(versions[2]) {
		t.Errorf("Expected 2.0.0: %s", versions[2])
	}
	if !(Version{2, 5, 0}).Equal(versions[3]) {
		t.Errorf("Expected 2.5.0: %s", versions[3])
	}
	if !(Version{3, 0, 0}).Equal(versions[4]) {
		t.Errorf("Expected 3.0.0: %s", versions[4])
	}
	if !(Version{3, 1, 0}).Equal(versions[5]) {
		t.Errorf("Expected 3.1.0: %s", versions[5])
	}
}

// EOF
