package version

import (
	"fmt"
	"strconv"
	"strings"
)

type Version struct {
	Major, Minor, Micro int
}

type Versions []Version

func ParseVersions(versions string) (result Versions) {
	parts := strings.Split(strings.TrimSpace(versions), "\n")

	result = make([]Version, len(parts))
	for i := 0; i < len(parts); i++ {
		result[i] = ParseVersion(parts[i])
	}
	return
}

func (v Versions) Len() int {
	return len(v)
}

func (v Versions) Swap(i, j int) {
	v[i], v[j] = v[j], v[i]
}

func (v Versions) Less(i, j int) bool {
	return v[i].Less(v[j])
}

var EmptyVersion = Version{0, 0, 0}

func ParseVersion(version string) (result Version) {
	parts := strings.SplitN(strings.TrimSpace(version), ".", 3)

	result.Major = must(strconv.Atoi(parts[0]))
	if len(parts) > 1 {
		result.Minor = must(strconv.Atoi(parts[1]))
	}
	if len(parts) > 2 {
		result.Micro = must(strconv.Atoi(parts[2]))
	}
	return
}

func (v Version) Equal(other Version) bool {
	return v.Major == other.Major && v.Minor == other.Minor && v.Micro == other.Micro
}

func (v Version) Less(other Version) bool {
	if v.Major < other.Major {
		return true
	} else if v.Major == other.Major {
		if v.Minor < other.Minor {
			return true
		} else if v.Minor == other.Minor {
			if v.Micro < other.Micro {
				return true
			}
		}
	}
	return false
}

func (v Version) String() string {
	return fmt.Sprintf("%d.%d.%d", v.Major, v.Minor, v.Micro)
}

func must(i int, err error) int {
	if err != nil {
		panic(err)
	}
	return i
}

// EOF
