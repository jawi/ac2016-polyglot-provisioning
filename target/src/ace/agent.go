package ace

import (
	"archive/zip"
	"bytes"
	"fmt"
	"io"
	"io/ioutil"
	golog "log"
	"net/http"
	"os"
	"sort"
	"strconv"
	"strings"

	"ace/dp"
	"ace/log"
	"ace/manifest"
	"ace/version"
)

type Agent struct {
	TargetName       string
	ServerAddr       string
	AuditLog         *log.EventLog
	InstalledVersion version.Version
	client           *http.Client
}

func NewAgent(serverAddr, targetName string) (agent *Agent, err error) {
	auditLog := log.NewLog(targetName, 1)

	agent = &Agent{
		targetName,
		serverAddr,
		&auditLog,
		version.EmptyVersion,
		&http.Client{},
	}

	if err = agent.initMetaData(); err != nil {
		return nil, err
	}

	return agent, nil
}

func (agent Agent) createURL(path string, args ...interface{}) string {
	urlSpec := fmt.Sprintf("http://%s/%s", agent.ServerAddr, path)
	return fmt.Sprintf(urlSpec, args...)
}

func (agent Agent) SyncAuditLogWithAce() (err error) {
	var queryResp, sendResp *http.Response

	queryURL := agent.createURL("auditlog/query?tid=%s&logid=%d", agent.TargetName, agent.AuditLog.Id)
	if queryResp, err = agent.client.Get(queryURL); err != nil {
		return
	}
	defer queryResp.Body.Close()

	var logData io.Reader
	if logData, err = agent.prepareFeedback(queryResp.Body); err != nil {
		return
	}

	sendURL := agent.createURL("auditlog/send")
	req, err := http.NewRequest("POST", sendURL, logData)
	req.Header.Set("Content-Type", "text/plain")

	if sendResp, err = agent.client.Do(req); err != nil {
		return
	}
	defer sendResp.Body.Close()

	return nil
}

func (agent Agent) prepareFeedback(r io.Reader) (io.Reader, error) {
	var data []byte
	var err error

	if data, err = ioutil.ReadAll(r); err != nil {
		return nil, err
	}

	parts := strings.Split(string(data), ",")

	// Sanity check
	if parts[0] != agent.TargetName || parts[1] != strconv.Itoa(agent.AuditLog.Id) {
		return nil, fmt.Errorf("Read query response of other target (%s/%d)?!", parts[0], parts[1])
	}

	knownRange := log.EmptyRange
	if len(parts) > 2 && parts[2] != "" {
		knownRange = log.ParseRangeSet(parts[2])
	}

	sendRange := log.EmptyRange
	var logData bytes.Buffer
	for _, event := range agent.AuditLog.GetEvents(knownRange) {
		sendRange.Add(event.EventID)

		event.ToCSV(&logData)
		logData.WriteByte('\n')
	}

	if !sendRange.IsEmpty() {
		golog.Printf("Server already has auditlog event(s): %s. Sending auditlog with IDs %s.", knownRange, sendRange)
	}

	return &logData, nil
}

func (agent *Agent) CheckAndDownloadUpdate(handler dp.DPHandler) (success bool, err error) {
	var queryResp, downloadResp *http.Response

	queryURL := agent.createURL("deployment/%s/versions/", agent.TargetName)
	if queryResp, err = http.Get(queryURL); err != nil {
		return false, err
	}
	defer queryResp.Body.Close()

	body, err := ioutil.ReadAll(queryResp.Body)
	if len(body) == 0 {
		// No versions available for us...
		return true, nil
	}

	versions := version.ParseVersions(string(body))
	sort.Sort(versions)

	var latest = version.EmptyVersion
	if len(versions) > 0 {
		latest = versions[len(versions)-1]
	}

	if !agent.InstalledVersion.Less(latest) {
		return true, nil
	}

	downloadURL := agent.createURL("deployment/%s/versions/%s", agent.TargetName, latest)
	if downloadResp, err = http.Get(downloadURL); err != nil {
		return false, err
	}
	defer downloadResp.Body.Close()

	golog.Printf("New update (version = %s) is available", latest)

	return agent.installUpdate(downloadResp.Body, downloadResp.ContentLength, handler)
}

func (agent *Agent) installUpdate(r io.Reader, size int64, handler dp.DPHandler) (success bool, err error) {
	var f *os.File
	if f, err = ioutil.TempFile("", "goa"); err != nil {
		return false, err
	}
	defer func() {
		f.Close()
		os.Remove(f.Name())

		agent.UpdateMetadata()
	}()
	if _, err = io.Copy(f, r); err != nil {
		return false, err
	}

	var rd *zip.ReadCloser
	if rd, err = zip.OpenReader(f.Name()); err != nil {
		return false, err
	}
	defer rd.Close()

	success = false

	var dpkg *dp.DeploymentPackage
	var m *manifest.Manifest
	var bsn = "fake.bsn"
	var latest = version.EmptyVersion

	content := make([]*zip.File, 0, 2)

	for _, f := range rd.File {
		if manifest.ManifestName == f.Name {
			if m, err = manifest.ReadManifestFromZip(f); err != nil {
				return
			}

			dpkg = dp.CreateDP(m)

			if dpkg.IsDeploymentPackage() {
				bsn, latest = dpkg.GetInfo()
			}
		} else {
			content = append(content, f)
		}
	}

	if dpkg == nil || latest.Equal(version.EmptyVersion) {
		return false, fmt.Errorf("No deployment package found!")
	}

	var output string

	agent.AuditLog.Add(log.DeploymentAdmin_Install, "name", bsn)

	success, output, err = handler(dpkg, content)

	if err != nil {
		agent.AuditLog.Add(log.Framework_Warning, "id", "-1", "msg", err.Error())
	} else if output != "" {
		agent.AuditLog.Add(log.Framework_Info, "id", "-1", "msg", output)
	}

	if success {
		agent.InstalledVersion = latest
	}

	// We're ready...
	agent.AuditLog.Add(log.DeploymentAdmin_Complete, "name", bsn, "success", strconv.FormatBool(success), "version", latest.String())

	return
}

func (agent *Agent) UpdateMetadata() (err error) {
	if err = ioutil.WriteFile(".meta/version", []byte(agent.InstalledVersion.String()), 0600); err != nil {
		return
	}

	var f *os.File
	if f, err = os.OpenFile(".meta/auditLog", os.O_WRONLY|os.O_TRUNC|os.O_CREATE, 0600); err != nil {
		return
	} else {
		defer f.Close()
		err = agent.AuditLog.Write(f)
	}

	return
}

func (agent *Agent) initMetaData() (err error) {
	if err = os.Mkdir(".meta", 0700); err != nil {
		if !os.IsExist(err) {
			return err
		}

		err = nil
	}

	if meta, err := ioutil.ReadFile(".meta/version"); err != nil {
		if !os.IsNotExist(err) {
			return err
		}
	} else {
		agent.InstalledVersion = version.ParseVersion(string(meta))
	}

	if f, err := os.Open(".meta/auditLog"); err != nil {
		if !os.IsNotExist(err) {
			return err
		}
	} else {
		defer f.Close()

		if err = agent.AuditLog.Read(f); err != nil {
			return err
		}
	}

	// Make sure there's at least one event present...
	agent.AuditLog.Add(log.Framework_Started)

	return
}

// EOF
