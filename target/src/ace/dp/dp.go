package dp

import (
	"archive/zip"

	"ace/manifest"
	"ace/version"
)

type DeploymentPackage struct {
	manifest.Manifest
}

type DPHandler func(dp *DeploymentPackage, files []*zip.File) (bool, string, error)

func CreateDP(m *manifest.Manifest) (*DeploymentPackage) {
	manifest := *m
	return &DeploymentPackage{manifest}
}

func (dp DeploymentPackage) IsDeploymentPackage() (ok bool) {
	if _, ok = dp.Main["DeploymentPackage-Version"]; ok {
		_, ok = dp.Main["DeploymentPackage-SymbolicName"]
	}
	return
}

func (dp DeploymentPackage) GetInfo() (bsn string, v version.Version) {
	bsn = dp.Main["DeploymentPackage-SymbolicName"]
	v = version.ParseVersion(dp.Main["DeploymentPackage-Version"])
	return
}

// EOF
