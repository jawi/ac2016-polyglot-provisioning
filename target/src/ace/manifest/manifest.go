package manifest

import (
	"archive/zip"
	"bufio"
	"bytes"
	"fmt"
	"io"
	"strings"
)

const (
	ManifestName string = "META-INF/MANIFEST.MF"
)

type Attributes map[string]string

type Manifest struct {
	Main    Attributes
	Entries map[string]Attributes
}

func NewManifest() *Manifest {
	var manifest Manifest
	manifest.Main = make(map[string]string)
	manifest.Entries = make(map[string]Attributes)
	return &manifest
}

func ReadManifest(r io.Reader) (manifest *Manifest, err error) {
	var buffer bytes.Buffer
	var part []byte
	var prefix bool
	var main bool = true
	var lastKey string

	manifest = NewManifest()
	entries := make(map[string]string)

	reader := bufio.NewReader(r)
	for err == nil {
		part, prefix, err = reader.ReadLine()
		if len(part) == 0 {
			if main {
				manifest.Main = entries
				main = false
			} else if _, ok := entries["Name"]; ok {
				manifest.Entries[entries["Name"]] = entries
			}
			entries = make(map[string]string)
			continue
		}

		start := 0
		for start < len(part) && part[start] == ' ' {
			start++
		}
		buffer.Write(part[start:])

		if prefix {
			continue
		}

		if start > 0 {
			// Continuation of last line...
			if lastKey == "" {
				return nil, fmt.Errorf("Invalid line continuation!")
			}

			entries[lastKey] = entries[lastKey] + buffer.String()
		} else {
			lastKey, err = parseManifestLine(entries, buffer.String())
			if err != nil {
				return nil, err
			}
		}
		buffer.Reset()
	}
	if err == io.EOF {
		err = nil
	}
	// Make sure we process any trailing data...
	if buffer.Len() > 0 {
		parseManifestLine(entries, buffer.String())
	}

	if _, ok := entries["Name"]; ok {
		manifest.Entries[entries["Name"]] = entries
	}

	return manifest, err
}

func ReadManifestFromZip(f *zip.File) (manifest *Manifest, err error) {
	var rc io.ReadCloser
	if rc, err = f.Open(); err != nil {
		return nil, err
	}
	defer rc.Close()

	return ReadManifest(rc)
}

func parseManifestLine(entries map[string]string, line string) (string, error) {
	pos := strings.Index(line, ":")
	if line != "" && pos < 0 {
		return "", fmt.Errorf("Invalid manifest line!")
	}

	key := strings.TrimSpace(line[0:pos])
	value := strings.TrimSpace(line[pos+1:])

	entries[key] = value

	return key, nil
}

// EOF
