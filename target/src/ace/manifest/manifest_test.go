package manifest

import (
	"bytes"

	"testing"
)

func TestParseManifestWithContinuedLines(t *testing.T) {
	input := `Manifest-Version: 1.0
Long-Header1: this-is-a-very-very-very-very-long-line-with-lots-of-us
  eless-data
Short-Header2: short data
Long-Header2: this-is-a-very-very-very-very-long-line-with-lots-of-us
  eless-data

Name: otherEntry
Another-Header: 25
`

	if m, err := ReadManifest(bytes.NewBufferString(input)); err != nil {
		t.Fatal(err)
	} else if len(m.Main) != 4 {
		t.Fatalf("Expected 4 entries, but got %d entries", len(m.Main))
	} else {
		if "this-is-a-very-very-very-very-long-line-with-lots-of-useless-data" != m.Main["Long-Header1"] {
			t.Fatalf("Header1 should not be '%s'", m.Main["Long-Header1"])
		}
		if m.Main["Long-Header1"] != m.Main["Long-Header2"] {
			t.Fatalf("Header2 should not be '%s'", m.Main["Long-Header2"])
		}
		if m.Main["Short-Header2"] != "short data" {
			t.Fatalf("Short-Header2 should not be '%s'", m.Main["Short-Header2"])
		}
		if attrs, ok := m.Entries["otherEntry"]; !ok {
			t.Fatalf("otherEntry should be present: '%v'", m.Entries)
		} else {
			if attrs["Another-Header"] != "25" {
				t.Fatalf("Another-Header should not be '%s'", attrs["Another-Header"])
			}
		}
	}
}
