package log

import (
	"encoding/gob"
	"fmt"
	"io"
	"regexp"
	"strings"
)

type EventLog struct {
	TargetID string
	Id       int
	Entries  []*Event
}

func NewLog(targetID string, id int) (e EventLog) {
	e.TargetID = targetID
	e.Id = id
	e.Entries = make([]*Event, 0, 10)
	return
}

func (e *EventLog) ParseCSV(log string) (err error) {
	for _, entryStr := range strings.Split(strings.TrimSpace(log), "\n") {
		entry := NewEvent()
		if err = entry.ParseCSV(entryStr); err != nil {
			return
		}

		if e.Id != entry.StoreID || e.TargetID != entry.TargetID {
			panic("Invalid log: invalid store ID or target ID found!")
		}

		e.addEvent(entry)
	}
	return
}

func (e *EventLog) Add(eventType int, properties ...string) {
	entry := NewEvent()
	entry.TargetID = e.TargetID
	entry.StoreID = e.Id
	entry.EventID = 1 + len(e.Entries)
	entry.Type = eventType

	for i := 0; i < len(properties); i += 2 {
		entry.Properties[properties[i]] = properties[i+1]
	}

	e.addEvent(entry)
}

func (e *EventLog) GetEventIDs() (rs RangeSet) {
	if len(e.Entries) == 0 {
		return EmptyRange
	}
	rs = NewRangeSet(e.Entries[0].EventID, e.Entries[0].EventID)
	for i := 1; i < len(e.Entries); i++ {
		rs.Add(e.Entries[i].EventID)
	}
	return
}

func (e *EventLog) GetEvents(excluded RangeSet) (entries []*Event) {
	entries = make([]*Event, 0, len(e.Entries))
	for _, event := range e.Entries {
		if !excluded.Contains(event.EventID) {
			entries = append(entries, event)
		}
	}
	return
}

type logHeader struct {
	TargetID   string
	Id         int
	EntryCount int
}

func (e *EventLog) Read(r io.Reader) (err error) {
	dec := gob.NewDecoder(r)

	var hdr logHeader
	if err = dec.Decode(&hdr); err != nil {
		return
	}

	if e.TargetID != hdr.TargetID {
		return fmt.Errorf("Invalid log: expected %s, got %s!", e.TargetID, hdr.TargetID)
	}
	if e.Id != hdr.Id {
		return fmt.Errorf("Invalid log: expected %d, got %d!", e.Id, hdr.Id)
	}

	for i := 0; i < hdr.EntryCount; i++ {
		var entry Event
		if err = dec.Decode(&entry); err != nil {
			return
		}
		e.addEvent(entry)
	}
	return
}

func (e *EventLog) Write(w io.Writer) (err error) {
	enc := gob.NewEncoder(w)

	hdr := &logHeader{e.TargetID, e.Id, len(e.Entries)}
	if err = enc.Encode(hdr); err != nil {
		return
	}

	for _, entry := range e.Entries {
		if err = enc.Encode(entry); err != nil {
			return
		}
	}
	return
}

func (e *EventLog) addEvent(entry Event) {
	e.Entries = append(e.Entries, &entry)
}

func encode(s string) string {
	re := regexp.MustCompile("(\\${1}|[,\r\n])")
	return re.ReplaceAllStringFunc(s, func(m string) string {
		if m == "$" {
			return "$$"
		} else if m == "," {
			return "$k"
		} else if m == "\n" {
			return "$n"
		} else if m == "\r" {
			return "$r"
		}
		return m
	})
}

func decode(s string) string {
	re := regexp.MustCompile("(\\$[\\$knr])")
	return re.ReplaceAllStringFunc(s, func(m string) string {
		if m == "$$" {
			return "$"
		} else if m == "$k" {
			return ","
		} else if m == "$n" {
			return "\n"
		} else if m == "$r" {
			return "\r"
		}
		return m
	})
}

// EOF
