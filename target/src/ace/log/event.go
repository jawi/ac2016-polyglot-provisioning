package log

import (
	"bytes"
	"strconv"
	"strings"
	"time"
)

const (
	// properties: id = bundle ID, msg = log message, type = error type
	Framework_Info int = 1001
	// properties: id = bundle ID, msg = log message, type = error type
	Framework_Warning int = 1002
	// properties: id = bundle ID, msg = log message, type = error type
	Framework_Error int = 1003
	// properties: (none)
	Framework_Refresh int = 1004
	// properties: (none)
	Framework_Started int = 1005
	// properties: (none)
	Framework_StartLevel int = 1006

	// properties: name = BSN of DP
	DeploymentAdmin_Install int = 2001
	// properties: name = BSN of DP
	DeploymentAdmin_Uninstall int = 2002
	// properties: name = BSN of DP, success = installation result, version = installed version
	DeploymentAdmin_Complete int = 2003
)

type Event struct {
	TargetID   string
	StoreID    int
	EventID    int
	Time       int
	Type       int
	Properties map[string]string
}

func NewEvent() (result Event) {
	result.Time = int(time.Now().Unix() * 1000)
	result.Properties = make(map[string]string)
	return
}

func (e *Event) ParseCSV(str string) (err error) {
	parseEntry(e, str)
	return
}

func (e *Event) ToCSV(buf *bytes.Buffer) {
	buf.WriteString(e.TargetID)
	buf.WriteByte(',')
	buf.WriteString(strconv.Itoa(e.StoreID))
	buf.WriteByte(',')
	buf.WriteString(strconv.Itoa(e.EventID))
	buf.WriteByte(',')
	buf.WriteString(strconv.Itoa(e.Time))
	buf.WriteByte(',')
	buf.WriteString(strconv.Itoa(e.Type))
	for k, v := range e.Properties {
		buf.WriteByte(',')
		buf.WriteString(k)
		buf.WriteByte(',')
		buf.WriteString(encode(v))
	}
}

func parseEntry(entry *Event, eventStr string) {
	parts := strings.Split(eventStr, ",")
	entry.TargetID = parts[0]
	entry.StoreID = must(strconv.Atoi(parts[1]))
	if len(parts) > 2 && parts[2] != "" {
		entry.EventID = must(strconv.Atoi(parts[2]))
	}
	if len(parts) > 3 && parts[3] != "" {
		entry.Time = must(strconv.Atoi(parts[3]))
	}
	if len(parts) > 4 && parts[4] != "" {
		entry.Type = must(strconv.Atoi(parts[4]))
	}
	if len(parts) > 5 && parts[5] != "" {
		for i := 5; i < len(parts); i += 2 {
			entry.Properties[parts[i]] = decode(parts[i+1])
		}
	}
}

func must(i int, err error) int {
	if err != nil {
		panic(err)
	}
	return i
}

// EOF
