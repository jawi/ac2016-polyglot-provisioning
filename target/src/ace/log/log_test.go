package log

import (
	"bytes"
	"reflect"

	"testing"
)

func TestReadWriteLog(t *testing.T) {
	var err error

	l := NewLog("my Target", 1)

	l.Add(Framework_Started)
	l.Add(Framework_Refresh)
	l.Add(DeploymentAdmin_Install, "name", "my BSN")
	l.Add(DeploymentAdmin_Complete, "name", "my BSN", "success", "true", "version", "1.0.0")

	var b bytes.Buffer
	if err = l.Write(&b); err != nil {
		t.Fatal(err)
	}

	l2 := NewLog("my Target", 1)
	if err = l2.Read(&b); err != nil {
		t.Fatal(err)
	}

	if !reflect.DeepEqual(l.Entries, l2.Entries) {
		t.Errorf("Entries not equal %q vs %q!", l.Entries, l2.Entries)
	}
}

func TestReadWriteEmptyLog(t *testing.T) {
	var err error

	l := NewLog("my Target", 1)

	var b bytes.Buffer
	if err = l.Write(&b); err != nil {
		t.Fatal(err)
	}

	l2 := NewLog("my Target", 1)
	if err = l2.Read(&b); err != nil {
		t.Fatal(err)
	}

	if !reflect.DeepEqual(l.Entries, l2.Entries) {
		t.Errorf("Entries not equal %q vs %q!", l.Entries, l2.Entries)
	}
}

func TestDecodeEncodeLogEntry(t *testing.T) {
	input := "1-4$k6-7$k9-10$k12-13$k15-16$k18-19$k21-22$k24-25$k27-30$k32$k$$MSG$r$n"
	expected := "1-4,6-7,9-10,12-13,15-16,18-19,21-22,24-25,27-30,32,$MSG\r\n"

	output := decode(input)
	if output != expected {
		t.Errorf("Decoded entries not equal %q vs %q!", output, expected)
	}

	output = encode(expected)
	if output != input {
		t.Errorf("Encoded entries not equal %q vs %q!", output, expected)
	}
}

// EOF
