package log

import (
	"fmt"
	"math"
	"strconv"
	"strings"
)

type RangeSet struct {
	rangeSet string
}

var EmptyRange RangeSet = NewRangeSet(0, 0)

func NewRangeSet(lowest, highest int) RangeSet {
	return RangeSet{formatRangeSet(lowest, highest)}
}

func ParseRangeSet(rangeSet string) RangeSet {
	return RangeSet{strings.TrimSpace(decode(rangeSet))}
}

func (rs RangeSet) Contains(id int) bool {
	for _, part := range strings.Split(rs.rangeSet, ",") {
		if isIncluded(part, id) {
			return true
		}
	}
	return false
}

func (rs *RangeSet) Add(id int) (result bool) {
	var newRangeSet []string
	result, newRangeSet = expandRangeSet(rs.rangeSet, id)

	// second pass: coerce adjacent range-sets
	for idx := 0; idx < len(newRangeSet); idx++ {
		curL, curU := getBoundaries(newRangeSet[idx])

		if idx > 0 {
			prevL, prevU := getBoundaries(newRangeSet[idx-1])
			if curL == prevU+1 {
				// join these two ranges
				newRangeSet[idx-1] = formatRangeSet(prevL, curU)
				newRangeSet = append(newRangeSet[:idx], newRangeSet[idx+1:]...)
			}
		}
	}

	rs.rangeSet = strings.Join(newRangeSet, ",")

	return
}

func (rs RangeSet) String() string {
	return rs.rangeSet
}

func (rs RangeSet) IsEmpty() bool {
	return rs.rangeSet == EmptyRange.rangeSet
}

func (rs RangeSet) Equals(other RangeSet) bool {
	return rs.rangeSet == other.rangeSet
}

func expandRangeSet(rangeSet string, id int) (result bool, newRangeSet []string) {
	result = false
	newRangeSet = make([]string, 0, 10)

	if rangeSet != EmptyRange.rangeSet {
		for _, part := range strings.Split(rangeSet, ",") {
			lower, upper := getBoundaries(part)

			if id+1 == lower && !result {
				newRangeSet = append(newRangeSet, formatRangeSet(id, upper))
				result = true
			} else if upper == id-1 && !result {
				newRangeSet = append(newRangeSet, formatRangeSet(lower, id))
				result = true
			} else if lower <= id && id <= upper && !result {
				newRangeSet = append(newRangeSet, part)
				result = true
			} else {
				if id < lower && !result {
					newRangeSet = append(newRangeSet, strconv.Itoa(id))
					result = true
				}

				newRangeSet = append(newRangeSet, part)
			}
		}
	}

	if !result {
		newRangeSet = append(newRangeSet, strconv.Itoa(id))
		result = true
	}

	return
}

func formatRangeSet(lower, upper int) string {
	if lower == 0 {
		return fmt.Sprintf("-%d", upper)
	}
	if upper >= math.MaxInt32 {
		return fmt.Sprintf("%d-", lower)
	}
	return fmt.Sprintf("%d-%d", lower, upper)
}

func isIncluded(rangeSet string, id int) bool {
	lower, upper := getBoundaries(rangeSet)
	return lower <= id && id <= upper
}

func getBoundaries(rangeSet string) (lower, upper int) {
	if strings.Contains(rangeSet, "-") {
		parts := strings.Split(rangeSet, "-")
		if parts[0] == "" && parts[1] == "" {
			panic("Invalid range set!")
		}
		if parts[0] != "" {
			lower = must(strconv.Atoi(parts[0]))
		} else {
			lower = 0
		}
		if parts[1] != "" {
			upper = must(strconv.Atoi(parts[1]))
		} else {
			upper = math.MaxInt32
		}
	} else {
		upper = must(strconv.Atoi(rangeSet))
		lower = upper
	}
	return
}

// EOF
