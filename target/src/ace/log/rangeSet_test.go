package log

import (
	"testing"
)

func TestDecodeLogRangeSet(t *testing.T) {
	input := "1-4$k6-7$k9-10$k12-13$k15-16$k18-19$k21-22$k24-25$k27-30$k32$r$n"
	expected := "1-4,6-7,9-10,12-13,15-16,18-19,21-22,24-25,27-30,32\r\n"

	output := decode(input)
	if output != expected {
		t.Errorf("Entries not equal %q vs %q!", output, expected)
	}

	rs := ParseRangeSet(input)
	if !rs.Contains(4) {
		t.Errorf("Expected 4 to be present!")
	}
	if rs.Contains(5) {
		t.Errorf("Expected 5 not to be present!")
	}
	if !rs.Contains(6) {
		t.Errorf("Expected 6 to be present!")
	}
}

func TestIsIncluded(t *testing.T) {
	rs := ParseRangeSet("1-3,5,7-9")

	expectedOutcomes := []bool{true, true, true, false, true, false, true, true, true, false}

	for idx, expected := range expectedOutcomes {
		if rs.Contains(idx+1) != expected {
			t.Errorf("%d should be %b!", idx+1, expected)
		}
	}
}

func TestAddToEmpty(t *testing.T) {
	rs := EmptyRange

	if !rs.Add(1) || "1" != rs.String() {
		t.Errorf("Expected '1', got: '%s'", rs)
	}

	if !rs.Add(2) || "1-2" != rs.String() {
		t.Errorf("Expected '1-2', got: '%s'", rs)
	}

	if rs.Equals(EmptyRange) {
		t.Errorf("Expected different version range!")
	}
}

func TestAdd(t *testing.T) {
	rs := ParseRangeSet("2")

	if !rs.Add(3) || "2-3" != rs.String() {
		t.Errorf("Expected '2-3', got: '%s'", rs)
	}

	if !rs.Add(9) || "2-3,9" != rs.String() {
		t.Errorf("Expected '2-3,9', got: '%s'", rs)
	}

	if !rs.Add(5) || "2-3,5,9" != rs.String() {
		t.Errorf("Expected '2-3,5,9', got: '%s'", rs)
	}

	if !rs.Add(7) || "2-3,5,7,9" != rs.String() {
		t.Errorf("Expected '2-3,5,7,9', got: '%s'", rs)
	}

	if !rs.Add(1) || "1-3,5,7,9" != rs.String() {
		t.Errorf("Expected '1-3,5,7,9', got: '%s'", rs)
	}

	if !rs.Add(6) || "1-3,5-7,9" != rs.String() {
		t.Errorf("Expected '1-3,5-7,9', got: '%s'", rs)
	}

	if !rs.Add(4) || "1-7,9" != rs.String() {
		t.Errorf("Expected '1-7,9', got: '%s'", rs)
	}

	if !rs.Add(8) || "1-9" != rs.String() {
		t.Errorf("Expected '1-9', got: '%s'", rs)
	}
}

func TestAddOpenEnded(t *testing.T) {
	rs := ParseRangeSet("2-")

	if !rs.Add(3) || "2-" != rs.String() {
		t.Errorf("Expected '2-', got: '%s'", rs)
	}

	if !rs.Add(9) || "2-" != rs.String() {
		t.Errorf("Expected '2-', got: '%s'", rs)
	}

	if !rs.Add(1) || "1-" != rs.String() {
		t.Errorf("Expected '1-', got: '%s'", rs)
	}
}

// EOF
