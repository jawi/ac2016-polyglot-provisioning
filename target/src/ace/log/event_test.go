package log

import (
	"testing"
)

func TestReadMinimalEvent(t *testing.T) {
	event := NewEvent()
	if err := event.ParseCSV("a,1,"); err != nil {
		t.Fatal(err)
	}

	if event.TargetID != "a" {
		t.Fatal("Unexpected targetID")
	}
	if event.StoreID != 1 {
		t.Fatal("Unexpected storeID")
	}
}

// EOF
