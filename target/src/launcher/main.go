package main

import (
	"archive/tar"
	"archive/zip"
	"bytes"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net"
	"os"
	"os/signal"
	"regexp"
	"strings"
	"time"

	"github.com/fsouza/go-dockerclient"

	"ace"
	"ace/dp"
)

var (
	serverAddr = flag.String("server", "localhost:8080", "Bind address and port")
	targetName = flag.String("target", "go_target", "The target identifier for ACE")
)

func createDockerTarball(w io.Writer, files []*zip.File) (err error) {
	tw := tar.NewWriter(w)
	defer func() {
		err = tw.Close()
	}()

	for _, f := range files {
		var rc io.ReadCloser
		if rc, err = f.Open(); err != nil {
			return
		}
		defer rc.Close()

		var content []byte
		if content, err = ioutil.ReadAll(rc); err != nil {
			return
		}

		hdr := &tar.Header{
			Name: f.Name,
			Mode: 0600,
			Size: int64(len(content)),
		}

		if strings.HasPrefix(f.Name, "Dockerfile") {
			hdr.Name = "Dockerfile"
		}

		if err = tw.WriteHeader(hdr); err != nil {
			return
		}
		if _, err = tw.Write(content); err != nil {
			return
		}
	}

	return
}

func DockerContentHandler(dp *dp.DeploymentPackage, files []*zip.File) (result bool, output string, err error) {
	if len(files) == 0 {
		return true, "Nothing to do", nil
	}

	bsn, version := dp.GetInfo()
	// name should only contain [a-z0-9_.-]
	name := regexp.MustCompile("[^a-z0-9_.-]+").
		ReplaceAllString(fmt.Sprintf("%s-%s", bsn, version), "_")

	var client *docker.Client
	client, err = docker.NewClient("unix:///var/run/docker.sock")
	if err != nil {
		return
	}
	// Sanity check: make sure we can reach Docker...
	if err = client.Ping(); err != nil {
		return
	}

	var id string
	var containers []docker.APIContainers
	var buf bytes.Buffer

	containers, err = client.ListContainers(docker.ListContainersOptions{
		All:     true,
		Filters: map[string][]string{"name": {name}},
	})

	if err != nil {
		return
	}

	if len(containers) > 0 {
		id = containers[0].ID
	} else {
		// Container does not exist: create one...
		var tarball bytes.Buffer
		if err = createDockerTarball(&tarball, files); err != nil {
			return
		}

		err = client.BuildImage(docker.BuildImageOptions{
			Name:                name,
			NoCache:             true,
			RmTmpContainer:      true,
			ForceRmTmpContainer: true,
			InputStream:         &tarball,
			OutputStream:        &buf,
		})

		output = buf.String()
		result = strings.Contains(output, "Successfully built")
		if !result {
			return
		}

		// try to create a container so we can run this image...
		cfg := docker.Config{
			AttachStdout: true,
			AttachStderr: true,
			Image:        name,
		}

		var container *docker.Container
		container, err = client.CreateContainer(docker.CreateContainerOptions{
			Name:   name,
			Config: &cfg,
		})

		if err != nil {
			return
		}

		id = container.ID
	}

	err = client.StartContainer(id, &docker.HostConfig{})
	if e, ok := err.(*docker.ContainerAlreadyRunning); ok && e.ID == id {
		output = output + "\nContainer is already running"
	} else if err != nil {
		output = output + "\nFailed to start container: " + err.Error()
		result = false
		return
	}

	// Pull the logs from the container...
	buf.Reset()

	err = client.Logs(docker.LogsOptions{
		Container:    id,
		OutputStream: &buf,
		Stdout:       true,
		Timestamps:   true,
		RawTerminal:  true,
	})

	log.Printf("Log for container %s: %s", id, buf.String())

	output = output + "\n" + buf.String()

	return
}

func main() {
	flag.Parse()

	log.SetPrefix("[agent] ")

	if len(*serverAddr) == 0 {
		log.Fatal("Missing server address and port!")
	}

	host, port, _ := net.SplitHostPort(*serverAddr)
	log.Printf("Connecting to ACE on %s:%s...", host, port)

	quit := make(chan struct{})

	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	go func() {
		for _ = range c {
			// sig is a ^C, handle it
			log.Printf("Shutting down...")
			close(quit)
		}
	}()

	var agent *ace.Agent
	var err error

	ticker := time.NewTicker(5 * time.Second)
	if agent, err = ace.NewAgent(*serverAddr, *targetName); err != nil {
		log.Printf("Error creating ACE agent: %s", err.Error())
		return
	}

	log.Printf("Agent starting...")

	for {
		select {
		case <-ticker.C:
			if err = agent.SyncAuditLogWithAce(); err != nil {
				log.Printf("Failed to sync audit log: %s", err.Error())
				continue
			}

			var success bool
			success, err = agent.CheckAndDownloadUpdate(DockerContentHandler)
			if err != nil {
				log.Printf("Failed to check or download update: %s", err.Error())
			} else if !success {
				log.Printf("Failed to check or download update: no specific reason...")
			}
		case <-quit:
			ticker.Stop()
			agent.UpdateMetadata()
			return
		}
	}
}

// EOF
