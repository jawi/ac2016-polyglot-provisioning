package org.apache.ace.client.repository.helper.docker;

import static org.apache.ace.client.repository.helper.docker.Activator.PROCESSOR;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.ace.client.repository.helper.ArtifactHelper;
import org.apache.ace.client.repository.helper.ArtifactPreprocessor;
import org.apache.ace.client.repository.helper.ArtifactRecognizer;
import org.apache.ace.client.repository.helper.ArtifactResource;
import org.apache.ace.client.repository.helper.base.VelocityArtifactPreprocessor;
import org.apache.ace.client.repository.object.ArtifactObject;
import org.apache.ace.connectionfactory.ConnectionFactory;

public class DockerArtifactHelper implements ArtifactHelper, ArtifactRecognizer {
	public static final String DOCKER_MIMETYPE = "application/vnd.docker.file";

	public static final String KEY_FILENAME = "filename";
	public static final String KEY_BASE_IMAGE = "baseImage";
	public static final String KEY_MAINTAINER = "maintainer";

	private volatile ArtifactPreprocessor m_artifactPreprocessor;
	private volatile ConnectionFactory m_connFactory;

	@Override
	public boolean canHandle(String mimetype) {
		return DOCKER_MIMETYPE.equals(mimetype);
	}

	@Override
	public boolean canUse(ArtifactObject object) {
		if (object == null) {
			return false;
		}
		return canHandle(object.getMimetype());
	}

	@Override
	public Map<String, String> checkAttributes(Map<String, String> attributes) {
		return attributes;
	}

	@Override
	public Map<String, String> extractMetaData(ArtifactResource artifact) throws IllegalArgumentException {
		String name = new File(artifact.getURL().getFile()).getName();
		String baseImage = "";
		String maintainer = "";

		String content = readDockerfile(artifact);
		Matcher m = Pattern.compile("FROM\\s+([^\n\r]+)").matcher(content);
		if (m.find()) {
			baseImage = m.group(1);
		}
		m = Pattern.compile("MAINTAINER\\s+([^\r\n]+)").matcher(content);
		if (m.find()) {
			maintainer = m.group(1);
		}

		Map<String, String> result = new HashMap<String, String>();
		result.put(ArtifactObject.KEY_PROCESSOR_PID, PROCESSOR);
		result.put(ArtifactObject.KEY_MIMETYPE, DOCKER_MIMETYPE);
		result.put(ArtifactObject.KEY_ARTIFACT_NAME, String.format("%s (%s)", baseImage, maintainer));
		result.put(KEY_FILENAME, name);
		result.put(KEY_BASE_IMAGE, baseImage);
		result.put(KEY_MAINTAINER, maintainer);
		return result;
	}

	@Override
	public <TYPE extends ArtifactObject> String getAssociationFilter(TYPE obj, Map<String, String> properties) {
		return String.format("(&(%s=%s)(%s=%s)(%s=%s))", KEY_FILENAME, obj.getAttribute(KEY_FILENAME), KEY_BASE_IMAGE,
				obj.getAttribute(KEY_BASE_IMAGE), KEY_MAINTAINER, obj.getAttribute(KEY_MAINTAINER));
	}

	@Override
	public <TYPE extends ArtifactObject> int getCardinality(TYPE obj, Map<String, String> properties) {
		return Integer.MAX_VALUE;
	}

	@Override
	public Comparator<ArtifactObject> getComparator() {
		return null;
	}

	@Override
	public String[] getDefiningKeys() {
		return new String[] { KEY_FILENAME, KEY_BASE_IMAGE, KEY_MAINTAINER };
	}

	@Override
	public String getExtension(ArtifactResource artifact) {
		return "";
	}

	@Override
	public String[] getMandatoryAttributes() {
		return new String[] { KEY_BASE_IMAGE, KEY_MAINTAINER };
	}

	@Override
	public ArtifactPreprocessor getPreprocessor() {
		return m_artifactPreprocessor;
	}

	@Override
	public String recognize(ArtifactResource artifact) {
		String content = readDockerfile(artifact);
		if (content.contains("FROM ") && content.contains("MAINTAINER ")) {
			return DOCKER_MIMETYPE;
		}
		return null;
	}

    /**
     * Called by dependency manager upon start of this component.
     */
    protected void start() {
        m_artifactPreprocessor = new VelocityArtifactPreprocessor(m_connFactory);
    }

    /**
     * Called by dependency manager upon stopping of this component.
     */
    protected void stop() {
        m_artifactPreprocessor = null;
    }

	private String readDockerfile(ArtifactResource artifact) {
		String content = "";
		try (InputStream is = artifact.openStream(); Scanner s = new Scanner(is, "utf-8")) {
			content = s.useDelimiter("\\Z").next();
		} catch (IOException e) {
			e.printStackTrace(); // XXX
		}
		return content;
	}
}
