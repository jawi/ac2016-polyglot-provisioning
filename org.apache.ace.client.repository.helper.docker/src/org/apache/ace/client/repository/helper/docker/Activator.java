package org.apache.ace.client.repository.helper.docker;

import static org.apache.ace.client.repository.helper.docker.AuxArtifactHelper.*;
import static org.apache.ace.client.repository.helper.docker.DockerArtifactHelper.*;

import java.util.Dictionary;
import java.util.Hashtable;

import org.apache.ace.client.repository.helper.ArtifactHelper;
import org.apache.ace.client.repository.helper.ArtifactRecognizer;
import org.apache.ace.client.repository.object.ArtifactObject;
import org.apache.ace.connectionfactory.ConnectionFactory;
import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Constants;

public class Activator extends DependencyActivatorBase {
	public static final String PROCESSOR = "org.apache.ace.deployment.rp.docker";

	@Override
	public void init(BundleContext context, DependencyManager dm) throws Exception {
		Dictionary<String, Object> props = new Hashtable<String, Object>();
		props.put(ArtifactObject.KEY_MIMETYPE, DOCKER_MIMETYPE);
		props.put(Constants.SERVICE_PID, PROCESSOR);

		String[] ifaces = new String[] { ArtifactHelper.class.getName(), ArtifactRecognizer.class.getName() };

		dm.add(createComponent()
				.setInterface(ifaces, props)
				.setImplementation(DockerArtifactHelper.class)
				.add(createServiceDependency().setService(ConnectionFactory.class).setRequired(true)));

		props = new Hashtable<String, Object>();
		props.put(ArtifactObject.KEY_MIMETYPE, new String[] { JAVA_MIMETYPE, PYTHON_MIMETYPE });
		props.put(Constants.SERVICE_PID, PROCESSOR);

		dm.add(createComponent()
				.setInterface(ifaces, props)
				.setImplementation(AuxArtifactHelper.class));
	}

}
