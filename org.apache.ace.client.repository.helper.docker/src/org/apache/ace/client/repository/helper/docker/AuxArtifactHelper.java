package org.apache.ace.client.repository.helper.docker;

import static org.apache.ace.client.repository.helper.docker.Activator.PROCESSOR;

import java.io.File;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

import org.apache.ace.client.repository.helper.ArtifactHelper;
import org.apache.ace.client.repository.helper.ArtifactPreprocessor;
import org.apache.ace.client.repository.helper.ArtifactRecognizer;
import org.apache.ace.client.repository.helper.ArtifactResource;
import org.apache.ace.client.repository.object.ArtifactObject;

public class AuxArtifactHelper implements ArtifactHelper, ArtifactRecognizer {
	public static final String PYTHON_MIMETYPE = "application/vnd.python.file";
	public static final String JAVA_MIMETYPE = "application/vnd.java.file";

	public static final String KEY_FILENAME = "filename";

	@Override
	public boolean canHandle(String mimetype) {
		return JAVA_MIMETYPE.equals(mimetype) || PYTHON_MIMETYPE.equals(mimetype);
	}

	@Override
	public boolean canUse(ArtifactObject object) {
		if (object == null) {
			return false;
		}
		return canHandle(object.getMimetype());
	}

	@Override
	public Map<String, String> checkAttributes(Map<String, String> attributes) {
		return attributes;
	}

	@Override
	public Map<String, String> extractMetaData(ArtifactResource artifact) throws IllegalArgumentException {
		String name = getFilename(artifact);
		String mimetype = recognize(artifact);

		Map<String, String> result = new HashMap<String, String>();
		result.put(ArtifactObject.KEY_PROCESSOR_PID, PROCESSOR);
		result.put(ArtifactObject.KEY_MIMETYPE, mimetype);
		result.put(ArtifactObject.KEY_ARTIFACT_NAME, name);
		result.put(KEY_FILENAME, name);
		return result;
	}

	@Override
	public <TYPE extends ArtifactObject> String getAssociationFilter(TYPE obj, Map<String, String> properties) {
		return String.format("(%s=%s)", KEY_FILENAME, obj.getAttribute(KEY_FILENAME));
	}

	@Override
	public <TYPE extends ArtifactObject> int getCardinality(TYPE obj, Map<String, String> properties) {
		return Integer.MAX_VALUE;
	}

	@Override
	public Comparator<ArtifactObject> getComparator() {
		return null;
	}

	@Override
	public String[] getDefiningKeys() {
		return new String[] { KEY_FILENAME };
	}

	@Override
	public String getExtension(ArtifactResource artifact) {
		String name = getFilename(artifact);
		if (name.endsWith(".java")) {
			return ".java";
		} else if (name.endsWith(".py")) {
			return ".py";
		}
		return "";
	}

	@Override
	public String[] getMandatoryAttributes() {
		return new String[0];
	}

	@Override
	public ArtifactPreprocessor getPreprocessor() {
		return null; // not supported
	}

	@Override
	public String recognize(ArtifactResource artifact) {
		String name = getFilename(artifact);
		return name.endsWith(".java") ? JAVA_MIMETYPE : name.endsWith(".py") ? PYTHON_MIMETYPE : null;
	}

	private String getFilename(ArtifactResource artifact) {
		return new File(artifact.getURL().getFile()).getName();
	}
}
