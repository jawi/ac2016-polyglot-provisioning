package org.apache.ace.client.repository.helper.docker;

import static org.apache.ace.client.repository.helper.docker.DockerArtifactHelper.*;
import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.net.URL;
import java.util.Map;

import org.apache.ace.client.repository.helper.ArtifactResource;
import org.apache.ace.client.repository.helper.docker.DockerArtifactHelper;
import org.junit.Test;

public class DockerArtifactHelperTest {

	@Test
	public void testExtractMetadataOk() throws Exception {
		ArtifactResource res = createMockObject("Dockerfile1");
		assertNotNull(res);

		Map<String, String> metadata = new DockerArtifactHelper().extractMetaData(res);
		assertNotNull(res);
		assertEquals("Dockerfile1", metadata.get(KEY_FILENAME));
		assertEquals("ubuntu:14.04", metadata.get(KEY_BASE_IMAGE));
		assertEquals("jawi", metadata.get(KEY_MAINTAINER));
	}

	private ArtifactResource createMockObject(String dockerFile) throws IOException {
		URL res = getClass().getResource(dockerFile);

		ArtifactResource result = mock(ArtifactResource.class);
		when(result.getURL()).thenReturn(res);
		when(result.openStream()).thenReturn(res.openStream());

		return result;
	}
}
